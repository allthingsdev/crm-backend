--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Ubuntu 14.1-2.pgdg20.04+1)
-- Dumped by pg_dump version 14.1 (Ubuntu 14.1-2.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: crm_users; Type: TABLE; Schema: public; Owner: admin
--

CREATE TABLE public.crm_users (
    user_id uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    user_name character varying(255) NOT NULL,
    first_name character varying(255) NOT NULL,
    middle_name character varying(255),
    last_name character varying(255),
    email character varying(255),
    phone_number character varying(255),
    password character varying(255),
    account_status character varying(255),
    created_at timestamp(0) with time zone DEFAULT NULL::timestamp with time zone,
    updated_at timestamp(0) with time zone DEFAULT NULL::timestamp with time zone,
    counter integer NOT NULL
);


ALTER TABLE public.crm_users OWNER TO admin;

--
-- Name: crm_users_counter_seq; Type: SEQUENCE; Schema: public; Owner: admin
--

CREATE SEQUENCE public.crm_users_counter_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.crm_users_counter_seq OWNER TO admin;

--
-- Name: crm_users_counter_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: admin
--

ALTER SEQUENCE public.crm_users_counter_seq OWNED BY public.crm_users.counter;


--
-- Name: crm_users_view_total_count; Type: VIEW; Schema: public; Owner: admin
--

CREATE VIEW public.crm_users_view_total_count AS
 SELECT count(crm_users.counter) AS total
   FROM public.crm_users;


ALTER TABLE public.crm_users_view_total_count OWNER TO admin;

--
-- Name: crm_users counter; Type: DEFAULT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.crm_users ALTER COLUMN counter SET DEFAULT nextval('public.crm_users_counter_seq'::regclass);


--
-- Data for Name: crm_users; Type: TABLE DATA; Schema: public; Owner: admin
--

COPY public.crm_users (user_id, user_name, first_name, middle_name, last_name, email, phone_number, password, account_status, created_at, updated_at, counter) FROM stdin;
176d1d52-bef5-4a47-9748-90d06906c901	geno	Elly	Marvel	Geno	odhiambogeno@gmail.com	+254702135219	$2y$10$aRMx8NvYpJmxZyz4ZWNCw.sTkEU.yXZKziDeAFSIfd1nPDTcD2HxO	active	2022-01-03 14:05:54+03	2022-01-03 14:05:54+03	1
95cb4496-ea7f-4d5d-85e6-5a1695b6ef1b	tom	Thomas	Tuchel	TTCh	ttch@gmail.com	+254702135210	$2y$10$kFlcb1iISGJ/NKfqQ/taGO6hSvzivxfHR9t7hePsHLKA1vHpOqINq	active	2022-01-08 06:09:52+03	2022-01-08 06:09:52+03	2
20352454-5331-4bff-b1cc-099102d5d1bc	sele	Selestine	Atieno	Were	saw@gmail.com	+254702135213	$2y$10$jxCPQiGLH7/R2YpUUUqyt.fCnGgR07OReYhl97zQACrTHkiQPs7Nq	active	2022-01-08 06:10:33+03	2022-01-08 06:10:33+03	3
544ea969-d7e1-45b2-b223-62a707b48074	admin	System	Atieno	Administrator	saw@gmail.com	+254702134513	$2y$10$4XK71KZdDVxKSFOSBHMCGuJ15xkllvQ/fO1zfVJQq.qR/Uwpt/mdi	active	2022-01-08 06:11:11+03	2022-01-08 06:11:11+03	4
a9e70f3e-dba2-430f-a757-658409583470	manager	Manager	All	MGR	mgr@gmail.com	+254702149913	$2y$10$i7pJF2olpPXde5uHKeBXiufSKpt..C.WJxYbimsS5yrrkVibRYhO.	active	2022-01-08 09:03:06+03	2022-01-08 09:03:06+03	5
56185af5-74a8-4810-81c4-286efc01df5a	accounts	Accounts	All	ACCT	acct@gmail.com	+254702499613	$2y$10$o6bGtV2xbh71bo45.BIJ7.CmcYZ34Ept3vRBez3akXqc.mk742/Ce	active	2022-01-08 09:03:34+03	2022-01-08 09:03:34+03	6
ac757bf2-f5b9-4b7c-be77-cbc0b8ad3ae2	paul	Paul	All	For	paf@gmail.com	+254702339713	$2y$10$Orv2fME8VeWPbkd60Hsf7O0SJZIFWmTk..0vZiOr0DqlbQ4PC9ol2	active	2022-01-08 09:04:08+03	2022-01-08 09:04:08+03	7
f87c1a1e-de53-4dfd-9264-4d2bef37dee8	biko	Biko	User	Alulu	baalulu@gmail.com	+254702133323	$2y$10$dsMnG4/VjTnA.Jr7wCIUeOIu61oVPlA8AZyPYYgA.AvzTn7ek8EC2	active	2022-01-13 14:25:16+03	2022-01-13 14:25:16+03	8
69655f4a-ed05-412e-8b4e-0be066d81e06	zulu	Zulu	User	Kalulu	baalulu@gmail.com	+254702149823	$2y$10$ti/UUxcXgJwt4Htvs3z1bOnKDfie/ovH0wCllhOzia7d.liC.u3Wa	active	2022-01-13 14:25:41+03	2022-01-13 14:25:41+03	9
069ccc50-f23f-4ffe-a26a-1696d69a85ba	chona	Chona	User	Tufu	tufu@gmail.com	+254704761823	$2y$10$TPt3gLnwKTYnqwclSyVCkug/PycRYSfJ5yxdjrPjp1iT7R0uoEpB2	active	2022-01-13 14:26:22+03	2022-01-13 14:26:22+03	10
1f5ef913-6bf7-47d0-b2ca-5c5164697b0e	wena	Muli	User	Wena	wena@gmail.com	+254704761823	$2y$10$zrazPWKUiKZicJO.ECziJ.fQ.tJ/ntgWAl1NvqlgRVWrFuyMTzvmu	active	2022-01-13 14:26:59+03	2022-01-13 14:26:59+03	11
259f338f-407a-410c-af16-c289ca7b50ba	dempsey	Cliff	User	Dempsey	cliff@gmail.com	+254704943723	$2y$10$NsHpyT8fiL92KyrN2YNEeu.sLkWMnbHzel.O7hxU1rFyV70rhFa4C	active	2022-01-13 14:27:51+03	2022-01-13 14:27:51+03	12
4fc57d0c-5573-44b9-ace4-45008de34e51	vost	Pro	User	Dempsey	provost@gmail.com	+254704943491	$2y$10$0SpvYLRnqmETokZlG6NhI.C4IrxnzakcBAiuDnpOilZFj0Ly5yfS6	active	2022-01-13 14:28:15+03	2022-01-13 14:28:15+03	13
9017afb2-bcc6-4d07-b885-65425b8477d9	con	Jure	User	Con	conjure@gmail.com	+254704943991	$2y$10$MYyES1J9SRl6dNGvLczqrOE0iBMYO9wg044LG.ymyTUwP1ls1Rtv6	active	2022-01-13 14:28:38+03	2022-01-13 14:28:38+03	14
c3f2e1ad-c5f9-4b24-a51d-38dae94fea45	malo	Ting	User	Badi	tingbadimalo@gmail.com	+254704943291	$2y$10$2lKvYDmVoH4Rk.CQp5hDmOZQy7y8U065bpZb5wf0p0F4oW6LSIqWy	active	2022-01-13 14:29:16+03	2022-01-13 14:29:16+03	15
aef7ad5d-c731-4d80-bd47-fc2e72ede89e	audi	Cars	User	Drive	audi@gmail.com	+254704990291	$2y$10$3HmRPRv7KdeYH14y8trfkO4xlzKGvfif7DCujOdqKZgUdC2YEsQuC	active	2022-01-13 14:29:43+03	2022-01-13 14:29:43+03	16
62988997-2437-49ce-a075-3b370734745d	devs	Rae	User	Devki	devki@gmail.com	+254704996491	$2y$10$gwlV6qlBPFaarjfRejBgl.0gDp/3eJshWm/DwVKj3elTZ5brcGaDC	active	2022-01-13 14:30:11+03	2022-01-13 14:30:11+03	17
6cdae396-4086-47cc-a46e-d64477ff4628	plus	Three	User	Ten	devki@gmail.com	+254704366491	$2y$10$BcWuDtYpBIR9EawBPHMvIOsiJkY61hRtmHz4UidQ3NaVoDE5JGpfa	active	2022-01-13 14:32:17+03	2022-01-13 14:32:17+03	18
8744862a-a271-4a5c-915c-dc14a603602e	quad	Three	User	Ruple	ruple@gmail.com	+254704471491	$2y$10$sTDDmqxO4b8jMz8pbF4T6OHb.WkK948LRT/uKx.DR1ruNymWPfw8K	active	2022-01-13 14:32:44+03	2022-01-13 14:32:44+03	19
ef2c5c11-e25e-4450-8815-26ce56d7330f	yaya	Toure	User	Kolo	yaya@gmail.com	+254704572291	$2y$10$GYSk/vCjPd1hmVUY7nLub.efXV1AdY8PxyVnhYqzcjV6qyvQAhq/e	active	2022-01-13 14:33:26+03	2022-01-13 14:33:26+03	20
ed0dfe70-53d7-4055-9f02-b1d3cdbe8399	blen	Brenda	User	Beln	blen@gmail.com	+254704457386	$2y$10$bOSal/Xs8Hfx22.EpJT7buwX.lNvQ0DaZHPVoI0VAXsGh/1KVKSs2	active	2022-01-13 14:33:56+03	2022-01-13 14:33:56+03	21
fd4b8420-d0e0-4935-ac79-3230148053b5	glee	Gee	User	Gleeson	gee@gmail.com	+254704457386	$2y$10$3JBtBfdShEBXTR9/qGGy9utG3CB19VMRFBYTszW7moKdefA.KZUWG	active	2022-01-13 18:22:06+03	2022-01-13 18:22:06+03	22
80b2bae9-90dc-4e51-98f7-33c6af0fc5a2	poto	Mac	User	User	mac@gmail.com	+254703987386	$2y$10$WnWtneW47M0oU4JY0HxvhOnGyzKL4FRreGY7vkIbd0OZds8qGElze	active	2022-01-13 18:22:42+03	2022-01-13 18:22:42+03	23
12f6feae-ef9c-43dc-84db-506480fe8ede	testing	Another	User	For	t30@gmail.com	+254703945086	$2y$10$ylOZQ/bcVd5jOMk42XZLHe6KgRL/TXfBhqQr/6NW0fv.ZSMvBu/kC	active	2022-01-13 18:23:56+03	2022-01-13 18:23:56+03	24
f25da57f-0837-450a-b505-4724d74aae46	cover	Cover	User	Prime	prime@gmail.com	+2547039445096	$2y$10$Jp/7xe304Pnd2Bkf0.Iao.f3h.UNPshyQGABD/6CudZIOKpiRI4U6	active	2022-01-13 18:24:22+03	2022-01-13 18:24:22+03	25
b58ae664-d844-40fa-baaa-ad5e63a45d2c	duce	Deuces	User	Tology	tall@gmail.com	+2547039445092	$2y$10$ZQwe4c7XrgfuXxxnWF3iXu9bHc1WHHD6bPQxacisN2cMweoVt/r66	active	2022-01-13 18:24:54+03	2022-01-13 18:24:54+03	26
08a5e602-cc5e-4305-b68d-4708fad6a7a1	cane	Jon	User	ktct	cane@gmail.com	+2547039445085	$2y$10$a67nfd3OvwZRJYxI0VQIR.OcSAw8WKv/UqLE1LnOROeQr5SmjVhZC	active	2022-01-13 18:25:25+03	2022-01-13 18:25:25+03	27
b56ccd8d-4604-41aa-9a4b-1e91bb1302b4	jon	Jon	User	Wick	wick@gmail.com	+2547039443495	$2y$10$5e8VtCwlWhd/Ek4I8gLImuNCmvfw3Kk6Db.llpKqTm0TvpdLOeWnm	active	2022-01-13 18:25:49+03	2022-01-13 18:25:49+03	28
3d43946d-cb29-4811-8155-5f22c6611150	once	Loser	User	Prote	once@gmail.com	+2547039443348	$2y$10$A0OK1kU44FUgo3OMhl5YmemcLh9guIKNixZW2rBZsWEAzKToA0LBK	active	2022-01-13 18:26:15+03	2022-01-13 18:26:15+03	29
f79133e5-f633-42ef-a0d4-95029587ee89	last	Dance	User	Last	lasted@gmail.com	+2547039238348	$2y$10$W8zzN72b7iML6zYXHmSLqu/ukV3RVeBCU3Tl5h4HAhJKe4Z79Bie6	active	2022-01-13 18:26:39+03	2022-01-13 18:26:39+03	30
7efb29ff-9f53-4d1e-91ad-e40408dc77f6	elly	Marvel Elly	User	Geno	gen@gmail.com	+2547039212098	$2y$10$.v9sNUpiwe40ZUrrk3Ru..3lfyUpYDHBeF3J47N.VE7nbcyoQYC4C	active	2022-01-13 18:28:22+03	2022-01-13 18:28:22+03	31
f7d07200-036b-4bd4-820d-7de9d4d756fc	stone	Stone Elly	User	SPrpe	stone@gmail.com	+2547039223968	$2y$10$Y2wg2sbnIN20Xhxp9FA9t.2NpCBwwqsZEeHgwlG4F0TLjVaaCfjDO	active	2022-01-13 18:29:04+03	2022-01-13 18:29:04+03	32
\.


--
-- Name: crm_users_counter_seq; Type: SEQUENCE SET; Schema: public; Owner: admin
--

SELECT pg_catalog.setval('public.crm_users_counter_seq', 32, true);


--
-- Name: crm_users crm_users_pkey; Type: CONSTRAINT; Schema: public; Owner: admin
--

ALTER TABLE ONLY public.crm_users
    ADD CONSTRAINT crm_users_pkey PRIMARY KEY (user_id);


--
-- PostgreSQL database dump complete
--

