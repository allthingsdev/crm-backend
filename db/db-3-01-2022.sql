DROP TABLE IF EXISTS "crm_users";

CREATE TABLE  "crm_users" (
        "user_id" UUID NOT NULL DEFAULT uuid_generate_v4(),
        "user_name" CHARACTER VARYING(255) NOT NULL,
        "first_name" CHARACTER VARYING(255) NOT NULL,
        "middle_name" CHARACTER VARYING(255) NULL,
        "last_name" CHARACTER VARYING(255) NULL,
        "email" CHARACTER VARYING(255) NULL,
        "phone_number" CHARACTER VARYING(255) NULL,
        "password" CHARACTER VARYING(255) NULL,
        "account_status" CHARACTER VARYING(255) NULL,
        "created_at" TIMESTAMP(0) WITH TIME ZONE NULL DEFAULT NULL,
        "updated_at" TIMESTAMP(0) WITH TIME ZONE NULL DEFAULT NULL,
        "counter" SERIAL,
        PRIMARY KEY ("user_id")
);