<?php

declare(strict_types=1);

namespace SimpleCRM;

use Auryn\Injector;

use Exception;
use FastRoute\Dispatcher;
use FastRoute\RouteCollector;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Narrowspark\HttpEmitter\SapiEmitter;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

use function SimpleCRM\Core\authenticate;
use function SimpleCRM\Core\getRoutes;
use function FastRoute\simpleDispatcher;


require __DIR__ . '/../vendor/autoload.php';
$config = require __DIR__ . '/core/config.php';


$sess = [];
$dbErr = "Database Exception - ";

$request = ServerRequest::fromGlobals();

$injector = new Injector();
require __DIR__ . '/core/dependencies.php';

$emitter = new SapiEmitter();

$server_params = $request->getServerParams();

date_default_timezone_set($config['tz']);
error_reporting(E_ALL);

$whoops = new Run();
if ($config['env'] !== 'prod') {
    $whoops->pushHandler(new PrettyPageHandler);
} else {
    $whoops->pushHandler(function ($e) {
        echo 'Todo: Friendly error page and send an email to the developer';
    });
}

$whoops->register();

if ($request->getMethod() === 'OPTIONS') {

    $response = new Response();

    $response = $response
        ->withAddedHeader('Access-Control-Allow-Headers', '*');

    $emitter->emit($response);

    exit;
}

$request_path = $request->getUri()->getPath();

$request_qs = $request->getUri()->getQuery();

$base_path = $config['base_path'];

if (strcmp($request_path, $base_path) !== 0) {

    $path = str_replace($base_path, '', $request_path);
} else {
    $path = '';
}

include __DIR__ . '/core/routes.php';
$allRoutes = getRoutes();

$routeDefinitionCallback = function (RouteCollector $r) {
    $routesArray = $GLOBALS['allRoutes'];
    foreach ($routesArray as $routes) {
        foreach ($routes as $route) {
            $r->addRoute($route[0], $route[1], $route[2]);
        }
    }
};


$dispatcher = simpleDispatcher($routeDefinitionCallback);

$routeInfo = $dispatcher->dispatch($request->getMethod(), $path);

$response = new Response();

switch ($routeInfo[0]) {
    case Dispatcher::NOT_FOUND:
        $response = new Response();
        $response->getBody()->write('Route not found');
        $response = $response->withStatus(404);

        break;

    case Dispatcher::METHOD_NOT_ALLOWED:
        $response = new Response();
        $response->getBody()->write('Method not allowed');
        $response = $response->withStatus(404);

        break;

    case Dispatcher::FOUND:

        $className = $routeInfo[1][0];
        $method = $routeInfo[1][1];
        $vars = $routeInfo[2];
        parse_str($request_qs, $qs);

        $vars['qs'] = $qs;
        // check if no authentication is allowed
        if (isset($routeInfo[1][2])) {
            $class = $injector->make($className);
            $response = $class->$method($vars);
        } else {
            $header = $request->getHeader('Authorization');

            if (isset($header[0])) {

                // remove postman bearer word
                if (strpos($header[0], 'Bearer') !== false) {
                    $jwt = str_replace('Bearer ', '', $header[0]);
                } else {
                    $jwt = $header[0];
                }

                include __DIR__ . '/core/check_jwt.php';

                try {

                    $is_auth = authenticate($jwt, $config['token']['key']);
                    $auth_model = $injector->make("SimpleCRM\Auth\Models\AuthModel");
                    $verifytoken = $auth_model->verifyToken($jwt);

                    if ($is_auth === true && $verifytoken) {
                        $class = $injector->make($className);
                        $response = $class->$method($vars);
                    } else {
                        $response = new Response();
                        $response->getBody()->write('Token Invalid - 1');
                        $response = $response->withStatus(404);
                    }
                } catch (Exception $e) {
                    $response = new Response();
                    $response->getBody()->write('Token Invalid - 2');
                    $response = $response->withStatus(404);
                }

            } else {
                $response = new Response();
                $response->getBody()->write('Not authorised to access this resource');
                $response = $response->withStatus(404);
            }
        }

        break;
}

$response = $response
    ->withAddedHeader('Access-Control-Allow-Origin', '*')
    ->withAddedHeader('Access-Control-Allow-Headers', '*');

$emitter->emit($response);
