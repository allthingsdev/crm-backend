<?php

declare(strict_types=1);

$injector->share($request);
$injector->alias('Psr\Http\Message\ResponseInterface', 'GuzzleHttp\Psr7\Response');

/**
 * the GuzzleHttp\Psr7\UploadedFile class requires 
 * initial values. The define method here is used to setup
 * these initial values.
 */
$injector->define('GuzzleHttp\Psr7\UploadedFile', [':streamOrFile' => 'default', ':size' => 0, ':errorStatus' => 0]);
$injector->alias('Psr\Http\Message\UploadedFileInterface', 'GuzzleHttp\Psr7\UploadedFile');


