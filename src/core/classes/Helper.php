<?php

declare(strict_types=1);

namespace SimpleCRM\Core\Classes;

class Helper
{

    public function __($string, $lang)
    {
    }


    public function getCountries()
    {
        return $GLOBALS['config']['countries'];
    }

    public function getLanguages()
    {
        return $GLOBALS['config']['lang'];
    }

    public function getCurrencies()
    {
        return $GLOBALS['config']['currencies'];
    }

    public function getTimezones()
    {
        return $GLOBALS['config']['timezones'];
    }

    public function getUserId()
    {

        //return $_SESSION["user_id"];
        if (isset($GLOBALS['sess']) && isset($GLOBALS['sess']['user_id'])) {
            return $GLOBALS['sess']['user_id'];
        } else {
            return '';
        }
    }

    public function getRequestJSON($request)
    {

        $post_data = $request->getBody()->getContents();
        $post_data = (array)json_decode($post_data);
        return $post_data;
    }
}
