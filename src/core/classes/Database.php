<?php

declare(strict_types=1);

namespace SimpleCRM\Core\Classes;

use Doctrine\DBAL\DriverManager;

class Database
{
    protected $db;

    public function __construct()
    {
        $this->db = $this->setConn();
    }

    public function setConn()
    {
        try {
            return DriverManager::getConnection($GLOBALS['config']['db']);
        } catch (\Exception $e) {
            echo 'Get Connection - Database Exception - ';
            print_r($e->getMessage()); die;
        }
    }

    public function getDb()
    {
        return $this->db;
    }
}
