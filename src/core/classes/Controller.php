<?php

declare(strict_types=1);

namespace SimpleCRM\Core\Classes;

use Exception;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\ServerRequest;
use Psr\Http\Message\ResponseInterface;

class Controller
{
    private $request;
    private $response;
    private $responseBody;
    public  $userId;

    public function __construct(
        ServerRequest $request,
        ResponseInterface $response,
        Helper $helper
    ) {
        $this->request = $request;
        $this->response = $response;
        $this->responseBody = $response->getBody();
        $this->helper = $helper;
        $this->setAppHeaders();

        $this->userId = $this->helper->getUserId();
    }

    private function setAppHeaders()
    {
        $config = require __DIR__ . '/../config.php';
        return $config['app_headers'];
    }

    public function checkLoggedIn(): bool
    {
        if (isset($_SESSION['isLoggedIn'])) {
            return true;
        }
        return false;
    }

    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Provides an instance of the
     * Response Handler class
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * Provides a Stream instance that is 
     * used in the creation of a http response
     * to render the UI
     * @var \SimpleCRM\Core\Classes\Renderer $renderer
     * @return StreamInterface
     */
    public function getResponseBody()
    {
        return $this->responseBody;
    }

    public function setResponse(
        array $data = [],
        string $status = '200',
        string $mime = 'application/json'
    ): ResponseInterface {
        /**
         * If returning data to a REST API the 
         * encode the data to JSON
         */
        if ($mime == 'application/json') {
            $this->responseBody->write(json_encode($data));
        } 

        return $this->getResponse()
            ->withBody($this->responseBody)
            ->withHeader('Content-Type', $mime)
            ->withStatus($status);
    }

    public function setErrorResponse(
        array $data = [],
        string $status = '400',
        string $mime = 'application/json'
    ) {

        return $this->setResponse($data, $status, $mime);
    }

    public function setRedirect(
        string $url,
        string $status = '302'
    ): ResponseInterface {
        $base = $GLOBALS['config']['base_path'];
        $response = $this->getResponse()
            ->withBody($this->responseBody)
            ->withHeader('Content-Type', 'text/plain')
            ->withAddedHeader('Location', $base . $url)
            ->withStatus($status);

        return $response;
    }


    /**
     * Get POST data as array
     */
    public function getPostData()
    {
        $post_data = $this->getRequest()->getParsedBody();

        if (!empty($post_data)) {
            return $post_data;
        } else {
            $post_data = $this->getRequest()->getBody()->getContents();

            return json_decode($post_data, true);
        }
    }

    /**
     * Validate image and Save file locally
     */
    public function saveUploadedFiles($module, $system = false)
    {
        
        $post_files = $_FILES;

        foreach ($post_files as $key => $file) {

            if ($file['size'] !== 0) {

                $filename = sprintf(
                    '%s.%s',
                    $this->create_uuid(),
                    pathinfo($file['name'], PATHINFO_EXTENSION)
                );

                $path = __DIR__ . '/../../../uploads/' . $GLOBALS['sess']['userId'] . '/' . $module . '/';
                if ($system === true) {
                    $path = __DIR__ . '/../../../uploads/' . $module . '/';
                }

                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
                // userId / module
                $file_location =  $path . $filename;
                move_uploaded_file($_FILES["file"]["tmp_name"], $file_location);
                return $filename;
            }
        }
        return false;
    }

    /**
     * function to generate a uuid
     */
    public function create_uuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        );
    }
}