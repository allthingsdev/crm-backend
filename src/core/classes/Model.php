<?php

declare(strict_types=1);

namespace SimpleCRM\Core\Classes;

use DateTime;
use Exception;
use Ramsey\Uuid\Uuid;

class Model
{
    private $db;
    public $now;
    public $userId;
    public $helper;

    public function __construct(Database $db, Helper $helper)
    {
        $this->db = $db->getDB();
        $this->now = date("Y-m-d H:i:s.u P", time());
        $this->helper = $helper;
        $this->userId = $this->helper->getUserId();
    }

    public function getDB()
    {
        return $this->db;
    }

    public function getUUID()
    {
        return Uuid::uuid4();
    }

    public function getAll($table)
    {
        try {
            $sql = 'SELECT * FROM ' . $table;
            return $this->db->fetchAllAssociative($sql);
        } catch (Exception $e) {
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    /**
     * Searches by exact value specified
     */
    public function getByField(string $table, string $field, $value)
    {
        try {
            $sql = "SELECT * FROM " . $table . " WHERE " . $field . " = ?";
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(1, $value);
            $result = $stmt->executeQuery()->fetchAllAssociative();
            if (isset($result)) {
                return $result;
            }
            return false;
        } catch (Exception $e) {
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    /**
     * Searches using the SQL wild card character
     */
    public function searchByField(string $table, string $field, $value)
    {
        try {
            $sql = "SELECT * FROM " . $table . " WHERE " . $field . " LIKE '%" . $value . "%'";
            $stmt = $this->db->prepare($sql);
            $result = $stmt->executeQuery()->fetchAllAssociative();

            if (isset($result)) {
                return $result;
            }
            return false;
        } catch (Exception $e) {
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }


    public function add(string $table, array $data)
    {
        try {
            $affectedRows = $this->db->insert($table, $data);
            return $affectedRows;
        } catch (Exception $e) {
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    public function update(string $table, array $data, string $id)
    {
        try {

            $this->db->update(
                $table,
                $data,
                array($id => $data[$id])
            );
        } catch (Exception $e) {
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    public function delete(string $table, string $idCol, string $id): int
    {
        try {
            $affectedRows = $this->db->delete($table, array($idCol => $id));
            return $affectedRows;
        } catch (Exception $e) {
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    public function deleteByParams(string $table, $conditions): int
    {
        try {
            $affectedRows = $this->db->delete($table, $conditions);
            return $affectedRows;
        } catch (Exception $e) {
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    public function getByParams(string $table, array $params)
    {
        try {

            //DriverManager::getConnection()->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

            $fields = [];
            $attributes = (isset($params['attributes'])) ? $params['attributes'] : "*";

            $sql = "SELECT $attributes FROM " . $table;

            if (array_key_exists('fields', $params)) {
                foreach ($params['fields'] as $key => $value) {

                    if ($value == null) {

                        $fields[] = $key . " IS NULL ";
                    } else {

                        $fields[] = $key . " = :" . $key;
                    }
                }

                if (!empty($fields)) {
                    $sql .= " WHERE " . implode(" AND ", $fields);
                }
            }



            if (isset($params['order'])) {
                foreach ($params['order'] as $key => $value) {
                    $sql .= " ORDER BY $key $value";
                }
            }

            if (isset($params['limit'])) {
                $sql .= " LIMIT " . $params['limit'];
            }

            $stmt = $this->db->prepare($sql);

            if (array_key_exists('fields', $params)) {
                foreach ($params['fields'] as $key => $value) {
                    $stmt->bindValue($key, $value);
                }
            }


            $result = $stmt->executeQuery()->fetchAllAssociative();
            if (isset($result)) {
                return $result;
            }
            return false;
        } catch (Exception $e) {
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    public function updateByParams(string $table, array $update_data, array $condition_data)
    {
        try {
            $this->db->update(
                $table,
                $update_data,
                $condition_data
            );
            return true;
        } catch (Exception $e) {
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    public function getRecordsRange($tbl, $from, $to, $cols = '*', $orderCol = 'counter', $order = 'asc')
    {
        try {
            $between = $from . ' and ' . $to;
            if ($from > $to) {
                $between = $to . ' and ' . $from;
            }
            $sql = "select $cols from $tbl where counter between $between order by $orderCol $order";
            return (array)$this->getBySql($sql, []);
        } catch (Exception $e) {
            die($e->getMessage());
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    public function getPage($tbl, $view, $pgSize, $pgNumber, $order = 'asc', $cols = '*', $orderCol = 'counter')
    {
        try {

            $args = [
                'tbl' => $tbl,
                'view' => $view,
                'size' => $pgSize,
                'no' => $pgNumber,
                'order' => $order,
                'cols' => $cols,
                'orderCol' => $orderCol,
            ];

            $totalRecords = $this->getRecordsTotal($view);

            $totalPages = ceil($totalRecords / $pgSize);

            if ($pgNumber > $totalPages) {
                die('PAGE DOES NOT EXIST');
                //page does not exist 
                return [];
            }

            if ($order === 'asc') {
                $maxRecord = ($pgSize * $pgNumber);
                $minRecord = ($maxRecord - $pgSize) + 1;
            } elseif ($order === 'desc') {
                if ($totalRecords < ($pgSize * $pgNumber) + 1) {
                    $minRecord = 1;
                    $maxRecord = fmod($totalRecords, (float)$pgSize);
                } else {
                    $maxRecord = $totalRecords - ($pgSize * $pgNumber) + 1;
                    $minRecord = ($maxRecord - $pgSize) - 1;
                }
            }
            $between = $minRecord . ' and ' . $maxRecord;
            $sql = "select $cols from $tbl where counter between $between order by $orderCol $order";

            $list = (array)$this->getBySql($sql);
            return [
                'pageCount' => $totalPages,
                'pageNumber' => (int)$pgNumber,
                'pageSize' => (int)$pgSize,
                'pageRecords' => $list,
            ];
        } catch (Exception $e) {
            die($e->getMessage());
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    public function getRecordsTotal($viewName)
    {
        try {
            $params = [
                'attributes' => 'total'
            ];
            $totalRecords = $this->getByParams($viewName, $params);
            return $totalRecords[0]['total'];
        } catch (Exception $e) {
            die($e->getMessage());
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    public function getBySql(string $sql, array $params = [])
    {
        try {

            $stmt = $this->db->prepare($sql);

            foreach ($params as $key => $value) {
                $stmt->bindValue($key, $value);
            }

            $result = $stmt->executeQuery()->fetchAllAssociative();
            if (isset($result)) {
                return $result;
            }
            return false;
        } catch (Exception $e) {
            throw new Exception($GLOBALS['dbErr'] . $e->getMessage());
        }
    }

    public function prepareLoggerData($data, $fields)
    {

        $logged_data = [];

        foreach ($fields as $field) {

            if (isset($data[$field])) {

                $logged_data[$field] = $data[$field];
            }
        }

        return $logged_data;
    }
}
