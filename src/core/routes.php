<?php

declare(strict_types=1);

namespace SimpleCRM\Core;

function getRoutes()
{
    $modules = $GLOBALS['config']['modules'];
    $moduleRoutes = [];
    foreach ($modules as $module) {
        // exclude the core module
        if (strpos($module, 'core') === false) {

            $routes = require __DIR__ . '/../' . $module . '/core/routes.php';

            $apiRoutes = [];

            foreach ($routes as $route) {
                $apiRoute[0] = $route[0];
                $apiRoute[1] = $GLOBALS['config']['api'].$route[1];
                $apiRoute[2] = $route[2];
                array_push($apiRoutes, $apiRoute);
                //print_r($api_route[1]); echo "\n";
            }
            //$apiRoutes[] = [ 'GET', '{bar:.*}',  ['SimpleCRM\Auth\Controllers\AuthController', 'anyRoute']];
            array_push($moduleRoutes, $apiRoutes);
        }
    }

    // $apiRoutes = [];
    // $apiRoutes[] = [ 'GET', '{bar:.*}',  ['SimpleCRM\Home\Controllers\HomeController', 'anyRoute']];
    // $moduleRoutes[] = $apiRoutes;
    return $moduleRoutes;
}

return getRoutes();
