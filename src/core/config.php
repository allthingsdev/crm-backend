<?php

declare(strict_types=1);

namespace SimpleCRM\Core;

$util = require_once __DIR__.'/util.php';

return [
    'env' => 'dev',
    'host' => 'http://localhost',
    // 'base_path' => '/apps/new/domains/ecombackend/backend/',
    'base_path' => '/m/crm/',
    'app_headers' => [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Headers' => '*',
        'Content-Type' => 'application/json; charset=UTF-8',
        'Access-Control-Allow-Methods' => 'OPTIONS,GET,POST,PUT,DELETE',
        'Access-Control-Allow-Headers' => 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With',
        'X-App-Name' => 'Simple CRM Application'
    ],
    'token' => [
        'key' => "SimpleCRM_Key",
        'issuer' => "dreamx_ecom", // 
        'audience' => "THE_AUDIENCE",
        'issue_date' => time(), // issued at
        'expire_after' => 20000, // tokenexpiry time in seconds
        
    ],
    'api' => 'api/v1',
    'db' => [
        'dbname' => 'crm',
        'user' => 'admin',
        'password' => 'a13',
        'host' => '127.0.0.1',
        'port' => '5432',
        'driver' => 'pdo_pgsql',
        'charset' => 'utf8'
    ],
    'modules' => [
        'core', 
        'admin/auth',
        'admin/users',
        'home',
    ],
    // 'lang' => $util['lang'],
    // 'currencies' => $util['currencies'],
    // 'timezones' => $util['timezones'],
    // 'countries' => $util['countries'],
    // App Timezone
    'tz' => 'Asia/Jerusalem'
];
