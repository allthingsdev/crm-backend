<?php

declare(strict_types=1);

namespace SimpleCRM\Home\Controllers;

use Exception;
use SimpleCRM\Core\Classes\Controller;

class HomeController
{
    private $controller;

    public function __construct(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function anyRoute()
    {
        try {
            
            $data = [
                'success' => 'Inside Any Route Method',
            ];
            return $this->controller->setResponse($data);
        } catch (Exception $e) {
            $data = ['error' => 'Registration Error - ' . $e->getMessage()];
            return $this->controller->setResponse($data);
        }
    }
}
