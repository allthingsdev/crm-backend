<?php

declare(strict_types=1);

namespace SimpleCRM\Home\Core;

$home_controller = 'SimpleCRM\Home\Controllers\HomeController';

return [
    ['GET', '/home', [$home_controller, 'index']],
    // SimpleCRM ROUTES
    ['GET', '/home/one', [$home_controller, 'one']],
    ['GET', '/home/{id}', [$home_controller, 'getOneById']],
    ['POST', '/home/add', [$home_controller, 'addOne']],
];