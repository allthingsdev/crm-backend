<?php

declare(strict_types=1);

namespace SimpleCRM\Admin\Users\Models;

use SimpleCRM\Core\Classes\Model;

class UserModel
{
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model;
    }
}