<?php

declare(strict_types=1);

namespace SimpleCRM\Admin\Users\Controllers;

use SimpleCRM\Core\Classes\Controller;
use Exception;
use Psr\Http\Message\ResponseInterface;
use SimpleCRM\Admin\Users\Models\UserModel;

class UserController
{
    private $controller;
    private $model;

    private $userTbl = 'crm_users';
    private $userTotalView = 'crm_users_view_total_count';

    public function __construct(Controller $controller, UserModel $model)
    {
        $this->controller = $controller;
        $this->model = $model->getModel();
    }

    public function getUser(array $id): ResponseInterface
    {
        try {
            $userData = $this->getUserDataArray($id['userId']);
            if (count($userData) == 1) {
                return $this->controller->setResponse($userData[0]);
            }
        } catch (Exception $e) {
            $data = ['error' => $e->getMessage()];
            return $this->controller->setErrorResponse($data);
        }
    }

    public function getUsers($var): ResponseInterface
    {
        try {
            if (array_key_exists('qs', $var)) {
                if (array_key_exists('pgSize', $var['qs'])) {
                    $userData = $this->getUsersPaged($var);
                } else {
                    $userData = $this->getUsersRange($var);
                }
            } else {
                $userData = $this->model->getAll($this->userTbl);
            }
            return $this->controller->setResponse((array)$userData);
        } catch (Exception $e) {
            die($e->getMessage());
            $data = ['error' => $e->getMessage()];
            return $this->controller->setErrorResponse($data);
        }
    }

    public function getUsersRange($var)
    {
        try {
            if (array_key_exists('qs', $var)) {
                $from = $var['qs']['from'];
                $to = $var['qs']['to'];
                $cols = '*';
                $orderCol = 'counter';
                $order = $var['qs']['order'];
                return $this->model->getRecordsRange($this->userTbl, $from, $to, $cols, $orderCol, $order);
            } else {
                return $this->model->getAll($this->userTbl);
            }
        } catch (Exception $e) {
            die($e->getMessage());
            $data = ['error' => $e->getMessage()];
            return $this->controller->setErrorResponse($data);
        }
    }

    public function getUsersPaged($var)
    {
        try {
            if (array_key_exists('qs', $var)) {
                $pgSize = $var['qs']['pgSize'];
                $pgNumber = $var['qs']['pgNumber'];
                $order = array_key_exists('order', $var['qs']) ? $var['qs']['order'] : 'asc';

                return $this->model->getPage($this->userTbl, $this->userTotalView, $pgSize, $pgNumber, $order);
            } else {
                return $this->model->getAll($this->userTbl);
            }
        } catch (Exception $e) {
            die($e->getMessage());
            $data = ['error' => $e->getMessage()];
            return $this->controller->setErrorResponse($data);
        }
    }

    public function getUserDataArray($userId)
    {
        try {
            $args = [
                'fields' => [
                    'user_id' => $userId
                ]
            ];
            return (array)$this->model->getByParams($this->userTbl, $args);
        } catch (Exception $e) {
            $data = ['error' => $e->getMessage()];
            return $this->controller->setErrorResponse($data);
        }
    }

    /**
     * Handles registration POST requests
     * - A 'POST' request takes the registration data from the
     * request and sends it to the authentication model
     * for processing and persistence
     */
    public function registerUser(): ResponseInterface
    {
        try {
            $postData = $this->controller->getPostData();

            $userData = $this->userDataToArray($postData);

            $this->model->add($this->userTbl, $userData);

            return $this->controller->setResponse($this->getUserDataArray($userData['user_id']));
        } catch (Exception $e) {
            $data = ['error' => $e->getMessage()];
            return $this->controller->setErrorResponse($data);
        }
    }

    private function userDataToArray(array $userData): array
    {
        $user = [
            'first_name' => $userData['first_name'],
            'middle_name' => $userData['middle_name'],
            'last_name' => $userData['last_name'],
            'user_name' => $userData['user_name'],
            'email' => $userData['email'],
            'password' => password_hash($userData['password'], PASSWORD_DEFAULT),
            'phone_number' => $userData['phone_number'],
            'account_status' => $userData['account_status'],
            'updated_at' => $this->model->now
        ];

        if (!isset($userData['userId'])) {
            $user['user_id'] = $this->model->getUUID();
            $user['created_at'] = $this->model->now;
        } else {
            // updating user details
            $user['user_id'] = $userData['user_id'];
            unset($user['password']);
        }
        return $user;
    }
    /**
     * Handles updating of user records
     * @param string $user_id
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function updateUser(array $id): ResponseInterface
    {
        try {
            $postData = $this->controller->getPostData();
            $userData = $this->userDataToArray($postData);

            $this->model->update($this->userTbl, $userData, $id['userId']);
            $data = ['success' => 'User Updated successfully'];
            return $this->controller->setResponse($data);
        } catch (Exception $e) {
            $data = ['error' => 'User Update Error - ' . $e->getMessage()];
            return $this->controller->setErrorResponse($data);
        }
    }

    /**
     * Handles deletion of user records
     * @param string $user_id
     * @return void
     */
    public function deleteUser($id)
    {
        // TODO ... check if the $user_id
        // is of the current logged in user
        if ($id['userId'] === $GLOBALS['sess']['userId']) {
            $data = ['error' => 'Cannot delete your own account'];
            return $this->controller->setResponse($data);
        }

        try {
            // TODO ... Check if any record was deleted
            $args = ['user_id' => $id['user_id']];
            $user = $this->model->getByParams($this->userTbl, $args);
            $this->model->deleteByParams($this->userTbl, $args);
            $data = [
                'success' => 'User Deleted successfully',
                'user' => $user
            ];
            return $this->controller->setResponse($data);
        } catch (Exception $e) {
            $data = ['error' => 'User Delete Error - ' . $e->getMessage()];
            return $this->controller->setErrorResponse($data);
        }
    }
}
