<?php

declare(strict_types=1);

namespace SimpleCRM\User\Core;

$userController = 'SimpleCRM\Admin\Users\Controllers\UserController';

return [
    ['GET', '/users', [$userController, 'getUsers',1]],
    ['POST', '/users', [$userController, 'registerUser', 1]],
    ['PUT', '/a/account/profile', [$userController, 'updateAdminProfile', 1]],
    ['GET', '/users/{userId}', [$userController, 'getUser', 1]],
    ['POST', '/users/{user_id:\d+}', [$userController, 'editUser']],
    ['DELETE', '/users/{user_id:\d+}', [$userController, 'deleteUser']],
    // user id must be a digit
];