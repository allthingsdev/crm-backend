<?php

declare(strict_types=1);

namespace SimpleCRM\Auth\Models;

use SimpleCRM\Core\Classes\Model;
use Exception;

/**
 * Handles Authentication
 */
class AuthModel
{
    private $model;
    private $table = 'users';
    private $token_tbl = 'tokens';

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function signInUser($credentials)
    {
        try {
            $user_details = $this->model->getByField($this->table, 'user_name', $credentials['username']);
            if (password_verify($credentials['password'], $user_details[0]['password'])) {

                /* The password is correct. */
                return $user_details[0];
            }
            return false;
        } catch (Exception $e) {
            $err = 'Unable to Sign User - Database Exception - ';
            throw new Exception($err . $e->getMessage());
        }
    }

    public function getUserDetails($user_id)
    {
        try {
            $user_details = $this->model->getByField($this->table, 'user_id', $user_id);
            return $user_details[0];
        } catch (Exception $e) {
            $err = '<h3>Unable to get user details - Database Exception - ';
            throw new Exception($err . $e->getMessage());
        }
    }

    public function changePassword($data, $user_id)
    {
        $details = $this->model->getByField($this->table, 'user_id', $user_id);

        if (!password_verify($data['current_password'], $details[0]['password'])) {
            throw new Exception("Password do not match");
        }

        $updateData = [
            'password' => password_hash($data['new_password'], PASSWORD_DEFAULT)
        ];

        return $this->model->updateByParams($this->table, $updateData, [
            'user_id' => $user_id
        ]);
    }

    public function verifyToken($jwt)
    {
        try {
            $token = $this->model->getByField($this->token_tbl, 'token', $jwt);
            if (!empty($token) && $this->model->now < $token[0]['expire_at']) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $err = 'Database Exception - ';
            throw new Exception($err . $e->getMessage());
        }
    }

    public function saveToken($data)
    {
        $token = $this->datatoArray($data);
        $this->model->add($this->token_tbl, $token);
    }

    public function datatoArray($t)
    {
        $token = [
            'token' => $t,
            'user_id' => isset($GLOBALS['sess']['user_id'])?$GLOBALS['sess']['user_id']:1,
            'created_at' => $this->model->now,
            'expire_at' => date("Y-m-d H:i:s", strtotime("+20000 seconds"))
        ];
        return $token;
    }
}
